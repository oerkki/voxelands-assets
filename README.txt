========================================================================
				README
========================================================================

ABOUT:
 This is the source repository for Voxelands (www.voxelands.com) art
  assets. The source files for blender models and gimp textures are kept
  here.

USE:
 If you add a file to this repo, add it to the ASSETS list below with a
  brief description, and your name or nickname.
 If you modify a file in this repo, add your name or nickname to the
  list of names that follow that file's name in this file.
 See the example asset item below.

LICENSE:
 All assets in this repository are released under the Creative Commons
  By Attribution Share Alike Unported license.

ASSETS:

 File: example_asset.blend
 Description: A non-existant example for this section of the readme file
 Creators/Contributors: darkrose

 File: kitty/kitty.blend
 Description: Blender source file for the kitty model, with armature and
 animation. Animations - walk, stand, sit, and attack/jump.
 Creators/Contributors: Gwinna (blendswap.com), elky

 File: kitty/kitty.xcf
 Description: Gimp source file for kitty textures. 4 layers, original
 siamese by Gwinna, grey tabby, ginger tabby and white kitty by elky.
 Creators/Contributors: Gwinna (blendswap.com), elky

 File: sheep/sheep.xcf
 Description: Gimp source file for sheep textures. 2 textures, normal
 and sheared.
 Creators/Contributors: p0ss (opengameart.org), elky

 File: sheep/sheared_sheep.blend
 Description: Blender source for the sheared sheep. Contains walk and
 stand/feed animations. The default sheep is a fattened version of this.
 Creators/Contributors: p0ss (opengameart.org), elky

 File: sheep/sheep.blend
 Description: Almost identical to sheep/sheared_sheep.blend, except some
 of the facets have been "fattened". Will probably need to be recreated
 eventually if armature/animation changes are needed, but is a shortcut
 until then. This is the default sheep.
 Creators/Contributors: p0ss (opengameart.org), elky

 File: tools/tool_texture_parts.xcf
 Description: Gimp source file for tool textures, one layer per part,
 tooltype, and overlay; includes ingots for colour reference.
 Creators/Contributors: darkrose
